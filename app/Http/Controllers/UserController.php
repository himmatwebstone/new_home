<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{
    public function index() {

    	return view('index');

    }


    public function submit(Request $request) {

        $request->validate(['first_name'       => 'required | max:10 |regex:/^[\pL\s\-]+$/u',
                            'last_name'        => 'required | max:10 |regex:/^[\pL\s\-]+$/u',
                            'username'         => 'required | max:10|regex:/^[\pL\s\-]+$/u',
                            'email'            => 'required | email',
                            'phone_number'     => 'required | max:10',
                            'password'         => 'required | min:6',
                            'confirm_password' => 'required',]);


    	$first_name   	  = $request->input('first_name');
    	$last_name    	  = $request->input('last_name');
    	$username     	  = $request->input('username');
    	$email        	  = $request->input('email');
    	$phone_number 	  = $request->input('phone_number');
    	$password     	  = $request->input('password');
    	$confirm_password = $request->input('confirm_password');

    	if ($password == $confirm_password){
            $password = md5($password);
            
	      	DB::insert("insert into  user(first_name, last_name, username, email, phone_number, password) values('$first_name','$last_name','$username','$email','$phone_number','$password')");

	    	echo "Record inserted successfully.<br/>";

    	}else{
    		echo "Password Should be match";
    	}
    	

    }

    public function login() {

        return view('login'); // html of login

    }


    public function login_submit(Request $request) {

        $request->validate(['email' => 'required',
                            'password'  => 'required']);

        $data = $request->input();

    }


    public function profile(Request $request) {

        $email = session()->get('email');
        $user  = User::whereEmail($email)->first();

        return view('profile', compact('user'));

        // // return view('profile');

    }


    public function logout(Request $request) {

       if (session()->has('email')) {

          session()->pull('email');

         }

         return redirect('login');

    }

    public function edit() {

        $email = session()->get('email');
        $user = User::whereEmail($email)->first();
       
        return view('update', compact('user'));

    }

    public function update_submit(Request $request) {

        echo "hello this is update call";
    }


} //end class