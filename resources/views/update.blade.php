<!DOCTYPE html>
<html lang="en">
<head>
  <title>Update Profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Update Profile</h2>
  
 <form action="update_submit" method="post">
                @csrf
    <div class="form-group">
      
        
      <label for="first_name">First Name:</label>
      <input type="text" class="form-control" id="first_name" placeholder="Enter First Name" name="first_name" value="{{$user->first_name}}">
    </div>
    <span style="color: red">@error('first_name'){{$message}}@enderror</span>

    <div class="form-group">
      <label for="last_name">Last Name:</label>
      
      <input type="text" class="form-control" id="last_name" placeholder="Enter Last Name" name="last_name" value="{{$user->last_name}}">
    </div>
    <span style="color: red">@error('last_name'){{$message}}@enderror</span>

    <div class="form-group">
      <label for="username">Username:</label>
      
      <input type="text" class="form-control" id="username" placeholder="Enter Username" name="username" value="{{$user->username}}">
    </div>
    <span style="color: red">@error('username'){{$message}}@enderror</span>

    <div class="form-group">
      <label for="email">Email:</label>
      
      <input type="text" class="form-control" id="email" placeholder="Enter email" name="email" value="{{$user->email}}">
    </div>
    <span style="color: red">@error('email'){{$message}}@enderror</span>

    <div class="form-group">
      <label for="phone_number">Phone Number:</label>
      
      <input type="text" class="form-control" id="phone_number" placeholder="Enter Phone Number" name="phone_number" value="{{$user->phone_number}}">
    </div>
    <span style="color: red">@error('phone_number'){{$message}}@enderror</span>

  

    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

</body>
</html>
