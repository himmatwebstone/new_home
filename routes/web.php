<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\UserAuth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'index']); // Render html signup page to root

Route::post('submit', [UserController::class, 'submit']); //submit of register

Route::get('login', [UserController::class, 'login']); // Render html login

Route::post('user', [UserAuth::class, 'userLogin']); // call controller UserAuth

Route::get('profile', function() { // Render and check validation

 if (!session()->has('email')) {

        return redirect('login');

       }
        return view('profile');

});

Route::get('profile', [UserController::class, 'profile'])->name('profile');// Render html of update page and data

// Route::get('login', function() { // Render and check validation

//  if (session()->has('email')) {

//         return redirect ('profile'); 

//        }

//         return view('login');
// });


Route::get('logout', [UserController::class, 'logout']); // Logout

Route::get('edit-profile', [UserController::class, 'edit'])->name('edit-profile');// Render html of update page and data

Route::post('update_submit', [UserController::class, 'update_submit']); // Submit data of update

// Route::get('profile', function() {

// 	if (session()->has('email')) {

//         return view('profile');

//        }

//         return view('profile');

// });

// Designer front-end developer

// HTML, Csss, Jvasct, Css, Phtotshop.....
